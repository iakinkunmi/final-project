// Pawn.cpp

#include "Pawn.h"

bool Pawn::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //check if the beginning posisition is legal
  if (start.first < 'A' || start.first > 'H' || start.second < '1' || start.second > '8') {
    return false;
  }
  
  //check if the destination is legal
  if (end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }
  //check if the path is reachable
  if (start.first - end.first != 0) {
    return false;
  }
  if (end.second - start.second == 0) {
    return false;
  }
  if (is_white() == true && end.second - start.second == 2 && start.second == '2') {
    return true;
  }
  if (is_white() == false && end.second - start.second == -2 && start.second == '7') {
    return true;
  }
  // ensures no backwards movement
  if (end.second - start.second == 1 && is_white() == true) {
    return true;
  }
  if (end.second - start.second == -1 && is_white() == false) {
    return true;
  }
  return false;
}

bool Pawn::legal_capture_shape(std::pair<char,char> start, std::pair<char,char> end) const {
  // check if start and end positions are valid first
  if (start.first < 'A' || start.first > 'H' || start.second < '1' || start.second > '8') {
    return false;
  }
  if (end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }
  // consider cases for white pawn
  if (is_white() == true) {
    if((start.first - end.first == 1 || start.first - end.first == -1) && start.second - end.second == -1) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    if ((start.first - end.first == 1 || start.first - end.first == -1) && start.second - end.second == 1) {
      return true;
    }
  }
  // otherwise, return false
  return false;
}
