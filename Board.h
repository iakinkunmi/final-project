#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <map>
#include "Piece.h"
#include "Pawn.h"
#include "Mystery.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"


class Board
{
	// Throughout, we will be accessing board positions using an std::pair< char , char >.
	// The assumption is that the first value is the row with values in {'A','B','C','D','E','F','G','H'} (all caps)
	// and the second is the column, with values in {'1','2','3','4','5','6','7','8'}

public:
	// Default constructor
	Board( void );


	// Board copy contructor
	Board(const Board& );

	// Destructor
	~Board(void);
	
	// Returns a const pointer to the piece at a prescribed location if it exists, or a NULL pointer if there is nothing there.
	const Piece* operator() ( std::pair< char , char > position ) const;

	// Attempts to add a new piece with the specified designator, at the given location.
	// Returns false if:
	// -- the designator is invalid,
	// -- the specified location is not on the board, or
	// -- if the specified location is occupied
	bool add_piece( std::pair< char , char > position , char piece_designator );

	//Attempt to remove the piece at the given location on the board
	//Return false if:
	// -- the specified location is invalid
	// -- there does not exist a piece at the position
	bool remove_piece( std::pair< char, char > position);

	// pawn promotion when it reaches the last row of the board
	bool pawn_promote( std::pair<char, char> position);
	
	// Displays the board by printing it to stdout
	void display( void ) const;

	// Returns true if the board has the right number of kings on it
	bool has_valid_kings( void ) const;

	// Check if the path is legal and clear
	// start is the position where the piece is at
	// end is the position where the piece is going to

	// this function only checks the squares along the path (i.e. not including start and end)
	bool legal_path(std::pair<char,char> start, std::pair<char,char> end) const;

	// return the position of a piece
	// return <'0','0'> if the piece is not found on the board
	std::pair<char,char> find(char c) const;

	// Check if the king is in check
	// white is true if it's checking for the white king
	bool check(bool white) const;

  
private:
	// The sparse map storing the pieces, keyed off locations
	std::map< std::pair< char , char > , Piece* > _occ;
};

// Write the board state to an output stream
std::ostream& operator << ( std::ostream& os , const Board& board );

#endif // BOARD_H
