// Rook.cpp

#include "Rook.h"

bool Rook::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  int colDiff = abs(start.first - end.first);
  int rowDiff = abs(start.second - end.second);
  //check if the start and end positions are legal
  if(start.first < 'A' || start.first > 'H') {return false;}
  if(start.second < '1' || start.second > '8') {return false;}
  if(end.first < 'A' || end.first > 'H') {return false;}
  if(end.second < '1' || end.second > '8') {return false;}
  //return true if the piece moves vertically
  if((colDiff > 0) && (rowDiff == 0)) {return true;}
  //return true if the piece moves horizontally
  if((colDiff == 0) && (rowDiff > 0)) {return true;}
  return false;
}
