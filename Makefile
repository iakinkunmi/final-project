# Final Project Makefile
# Laura Bao | rbao2
# Lingxi Shang | lshang2
# Ife Akinkunmi | iakinku1

CC = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic -g
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

chess: main.cpp main.o Bishop.o King.o Queen.o Rook.o Pawn.o Knight.o Chess.o Board.o CreatePiece.o
	$(CC) -o chess main.o Bishop.o King.o Queen.o Rook.o Pawn.o Knight.o Chess.o Board.o CreatePiece.o

test1: test1.cpp CreatePiece.o Bishop.o King.o Queen.o Rook.o Pawn.o Knight.o
	$(CC) $(CFLAGS) -o test1 test1.cpp CreatePiece.o Bishop.o King.o Queen.o Rook.o Pawn.o Knight.o

test_board: test_board.cpp CreatePiece.o Board.o Bishop.o King.o Queen.o Rook.o Pawn.o Knight.o
	$(CC) $(CFLAGS) -o test_board test_board.cpp CreatePiece.o Board.o Bishop.o King.o Queen.o Rook.o Pawn.o Knight.o

main.o: main.cpp *.cpp *.h
	$(CC) $(CFLAGS) -c main.cpp

Board.o: Board.cpp Board.h
	g++ -c Board.cpp -Wall -Wextra -std=c++11 -pedantic 

CreatePiece.o: CreatePiece.cpp *.h 
	g++ -c CreatePiece.cpp -Wall -Wextra -std=c++11 -pedantic 

Chess.o: Chess.cpp Chess.h
	g++ -c Chess.cpp -Wall -Wextra -std=c++11 -pedantic 

Bishop.o: Bishop.cpp Bishop.h
	g++ -c Bishop.cpp -Wall -Wextra -std=c++11 -pedantic

King.o: King.cpp King.h
	g++ -c King.cpp -Wall -Wextra -std=c++11 -pedantic

Queen.o: Queen.cpp Queen.h
	g++ -c Queen.cpp -Wall -Wextra -std=c++11 -pedantic

Rook.o: Rook.cpp Rook.h
	g++ -c Rook.cpp -Wall -Wextra -std=c++11 -pedantic

Pawn.o: Pawn.cpp Pawn.h
	g++ -c Pawn.cpp -Wall -Wextra -std=c++11 -pedantic

Knight.o: Knight.cpp Knight.h
	g++ -c Knight.cpp -Wall -Wextra -std=c++11 -pedantic

clean:
	rm -f *.o vgcore* chess test1 test_board
