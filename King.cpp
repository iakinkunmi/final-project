// King.cpp

#include "King.h"
#include <cmath>

bool King::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  int colDiff = abs(start.first-end.first);
  int rowDiff = abs(start.second-end.second);
  //check if the start and end positions are legal
  if(start.first < 'A' || start.first > 'H') {return false;}
  if(start.second < '1' || start.second > '8') {return false;}
  if(end.first < 'A' || end.first > 'H') {return false;}
  if(end.second < '1' || end.second > '8') {return false;}
  //check if the king only moves by one square
  if((colDiff == 1) && (rowDiff == 1)) {return true;}
  if((colDiff == 0) && (rowDiff == 1)) {return true;}
  if((colDiff == 1) && (rowDiff == 0)) {return true;}
  return false;
}
