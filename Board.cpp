#include <iostream>
#include <utility>
#include <map>
#include <string>
#ifndef _WIN32
#endif // !_WIN32
#include "Terminal.h"
#include "Board.h"
#include "CreatePiece.h"

using std::map;
using std::pair;
using std::make_pair;
using std::string;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board( void ){}

const Piece* Board::operator()( std::pair< char , char > position ) const
{
  //return NULL if the piece is not found
  map<pair<char,char>, Piece*>::const_iterator it = _occ.find(position);
  if (it == _occ.cend()) {
    return NULL;
  }
  //return piece pointer at the position
  if(it->second != NULL) {
    return it->second;
  }
  return NULL;
}

// Copy constructor
Board::Board(const Board& orig) {
  for(map<pair<char,char>, Piece*>::const_iterator it = orig._occ.cbegin();
      it != orig._occ.cend();
      ++it) {
    _occ[it->first] = create_piece(it->second->to_ascii());
  }
}

// Destructor
Board::~Board(void) {
  for (char i = '8'; i >= '1'; i--) {
    for (char j = 'A'; j <= 'H'; j++) {
      remove_piece(std::pair< char , char > (j, i));
    }
  }
}

bool Board::add_piece( std::pair< char , char > position , char piece_designator )
{
  //if location is correct
  if (position.first < 'A' || position.first > 'H' || position.second < '1' || position.second > '8') {
    return false;
  }

  //if the location is occupied
    if (_occ[position] != NULL) {
     return false;
   }
	_occ[ position ] = create_piece( piece_designator );
  //check if the piece is valid
  if (_occ[position] == NULL) {
    return false;
  }
	return true;
}

bool Board::remove_piece(pair<char, char> position)
{
  // check if position valid
  if (position.first < 'A' || position.first > 'H' || position.second < '1' || position.second > '8') {
    return false;
  }
  // return false if position is not occupied
  if (_occ.find(position) == _occ.end()) {
    return false;
  }
  // remove the key-value pair from the map
  delete _occ[position];
  _occ.erase(position);
  return true;
}

bool Board::pawn_promote(pair<char, char> position) {
  //check if position is the last row
  if (position.first < 'A' || position.first > 'H' || (position.second != '1' && position.second != '8')) {
    return false;
  }
  // promotion of a white pawn at row 8
  if (_occ[position]->to_ascii() == 'P' && position.second == '8') {
    return (remove_piece(position) && add_piece(position, 'Q'));
  }
  // promotion of a black pawn at row 1
  if (_occ[position]->to_ascii() == 'p' && position.second == '1') {
    return (remove_piece(position) && add_piece(position, 'q'));
  }
  return false;
}


//This function omits checking if the start and end positions
//It checks after legal-move-shape or legal-capture-shape in chess
bool Board::legal_path(pair<char,char> start, pair<char,char> end) const{

  // check when the piece moves horizontally to the east
  if (start.second == end.second && start.first < end.first) {
    int n = start.first + 1;
    while(n < end.first) {
      if(_occ.find(make_pair((char)n,start.second)) != _occ.end()) {
	return false;
      }
      n++;
    }
    return true;
  }
  // check when the piece moves horizontally to the west
  if (start.second == end.second && start.first > end.first) {
    int n = start.first - 1;
    while (n > end.first) {
      if(_occ.find(make_pair((char)n,start.second)) != _occ.end()) {
        return false;
      }
      n--;
    }
    return true;
  }
  // check when the piece moves vertically to the north
  if (start.first == end.first && start.second < end.second) {
    int n = start.second + 1;
    while (n < end.second) {
      if(_occ.find(make_pair(start.first, (char)n)) != _occ.end()) {
	return false;
      }
      n++;
    }
    return true;
  }
  // check when the piece moves vertically to the south
  if (start.first == end.first && start.second > end.second) {
    int	n = start.second - 1;
    while (n > end.second) {
      if(_occ.find(make_pair(start.first, (char)n)) != _occ.end()) {
        return false;
      }
      n--;
    }
    return true;
  }
  // check when the piece moves diagonally to north-east or south-west
  if (start.first - end.first == start.second - end.second) {
    //if the piece moves to north-east
    //both row and column increment
    if (start.first < end.first) {
      int i = start.first + 1;
      int j = start.second + 1;
      while (i < end.first) {
	if (_occ.find(make_pair((char)i,(char)j)) != _occ.end()) {
	  return false;
	}
	i++;
	j++;
      }
      return true;
    }
    //if the piece moves to south-west
    //both row and column decrement
    else {
      int i = start.first - 1;
      int j = start.second - 1;
      while (i > end.first) {
	if (_occ.find(make_pair((char)i, (char)j)) != _occ.end()) {
	  return false;
	}
	i--;
	j--;
      }
      return true;
    }
  }

  // check when the piece moves diagonally to north-west or south-east
  if (start.first - end.first + start.second - end.second == 0) {
    // check when the piece moves to south-east
    // rows decrement and columns increment
    if (start.first < end.first) {
      int i = start.first + 1;
      int j = start.second - 1;
      while (i < end.first) {
        if (_occ.find(make_pair((char)i,(char)j)) != _occ.end()) {
          return false;
        }
        i++;
        j--;
      }
      return true;
    }
    // check when the piece moves to north-west
    // rows increment and columns decrement
    else {
      int i = start.first - 1;
      int j = start.second + 1;
      while (i > end.first) {
        if (_occ.find(make_pair((char)i, (char)j)) != _occ.end()) {
          return false;
        }
        i--;
        j++;
      }
      return true;
    }
  }
  return true;
}

      
    

bool Board::has_valid_kings( void ) const
{
  int i = 0;
  for (map<pair<char,char>, Piece*>::const_iterator it = _occ.cbegin();
       it != _occ.cend();
       ++it) {
    // check for the black king
    if (it->second->to_ascii() == 'k') {
      i++;
    }
    // check for the white king
    if (it->second->to_ascii() =='K') {
      i++;
    }
  }
  if (i==2) {
  return true;
  }
  return false;

}

void Board::display( void ) const
{
  Terminal::color_all(true, Terminal::Color::RED, Terminal::Color::DEFAULT_COLOR);
  std::cout << *this;
  Terminal::set_default();
}

pair<char,char> Board::find(char c) const {
  for(map<pair<char,char>, Piece*>::const_iterator it = _occ.cbegin();
      it != _occ.cend();
      ++it) {
    // return position if found on board
    if (it->second->to_ascii() == c) {
      return it->first;
    }
  }
  // return <0,0> if not found
  return make_pair('0','0');
}

bool Board::check(bool white) const {
  //check if in check for the white king
  if(white) {
    pair<char,char> pos = find('K');
    for(map<pair<char,char>, Piece*>::const_iterator it = _occ.cbegin();
	it != _occ.cend();
	++it) {
      //check all the black pieces
      //check if the piece can capture the white king and the path is legal
      if (islower(it->second->to_ascii())
	  && it->second->legal_capture_shape(it->first,pos)
	  && legal_path(it->first,pos)) {
	    return true;
	  }
      }
    return false;
  }

  //check if in check for the black king
  else {
    pair<char,char> pos = find('k');
    for(map<pair<char,char>, Piece*>::const_iterator it = _occ.cbegin();
        it != _occ.cend();
        ++it) {
      //check all the white pieces
      //check if the piece can capture the black king and the path is legal   
      if (isupper(it->second->to_ascii())
          && it->second->legal_capture_shape(it->first,pos)
          && legal_path(it->first,pos)) {
            return true;
          }
      } 
    return false;
  }  
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Board& board )
{
	for( char r='8' ; r>='1' ; r-- )
	{
		for( char c='A' ; c<='H' ; c++ )
		{
			const Piece* piece = board( std::pair< char , char >( c , r ) );
			if( piece ) os << piece->to_ascii();
			else        os << '-';
		}
		os << std::endl;
	}
	return os;
}
