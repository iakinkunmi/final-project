// Bishop.cpp
// cs220 Final Project
// Lingxi Shang | lshang2
// Laura Bao | rbao2
// Ife Akinkunmi | iakinku1

#include "Bishop.h"

bool Bishop::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //check if the beginning posisition is legal
  if (start.first < 'A' || start.first > 'H' || start.second < '1' || start.second > '8') {
    return false;
  }
  
  //check if the destination is legal
  if (end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }
  //check if the path is reachable
  int moving_col = start.first - end.first;
  int moving_row = start.second - end.second;
  
  if (moving_row - moving_col == 0 ||
      moving_row + moving_col == 0 ||
      moving_col - moving_row == 0 ||
      moving_col + moving_row == 0) {
    return true;
  }
  
  return false;
}
