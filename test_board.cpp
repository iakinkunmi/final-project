//test1.cpp

#include <iostream>
#include <cassert>
#include "Rook.h"
#include "King.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "Pawn.h"
#include "CreatePiece.h"
#include "Board.h"


using std::make_pair; using std::pair;

int main() {

  Board _board;
  //add new one
  assert(_board.add_piece(pair<char,char> ('A', '1'), 'K'));

  assert(!_board.pawn_promote(make_pair('A', '1')));

  assert(_board.remove_piece(make_pair('A', '1')));
  
  assert(_board.add_piece(pair<char,char> ('B', '1'), 'k'));
  assert(_board.remove_piece(make_pair('B', '1')));
  //assert(_board.has_valid_kings());
  /*
  assert(_board(make_pair('A','1')));
  //check if can recognize different location
  assert(!_board.add_piece(pair<char,char> ('A', '1'), 'R'));
  //check diff pieces
 assert(_board.add_piece(pair<char,char> ('A', '6'), 'p'));
 assert(_board.add_piece(pair<char,char> ('B', '1'), 'N'));
 assert(_board.add_piece(pair<char,char> ('B', '6'), 'n'));
 assert(_board.add_piece(pair<char,char> ('C', '3'), 'R'));
 assert(!_board.add_piece(pair<char,char> ('C', '3'), 'r'));
 assert(_board.remove_piece(make_pair('C', '3')));
 assert(!_board.remove_piece(pair<char,char> ('D', '4')));
 //check pawn promotion
 assert(!_board.pawn_promote(make_pair('A', '6')));
 assert(!_board.pawn_promote(make_pair('B', '1')));
 assert(_board.add_piece(make_pair('H', '8'), 'P'));
 assert(_board.pawn_promote(make_pair('H','8')));
 //check the king
 assert(!_board.has_valid_kings());
  assert(_board.add_piece(pair<char,char> ('A', '3'), 'K'));
  assert(!_board.has_valid_kings());
  assert(_board.add_piece(pair<char,char> ('E', '5'), 'k'));
  assert(_board.has_valid_kings());
  //check operator
  const Piece*  test = _board(make_pair('A', '1'));
  assert(test->to_ascii() == 'P');
  const Piece*  test2 = _board(make_pair('A', '5'));
  assert(!test2);
  //check path_legal
  assert(_board.legal_path(make_pair('A','1'),make_pair('A','3')));
  assert(!_board.legal_path(make_pair('A','6'),make_pair('A','1')));
  assert(_board.legal_path(make_pair('A','3'),make_pair('A','4')));
  assert(_board.add_piece(make_pair('C','3'),'q'));
  assert(!_board.legal_path(make_pair('H','8'),make_pair('A','1')));
  assert(_board.legal_path(make_pair('A','1'),make_pair('C','3')));
  //test find function
  assert(_board.find('P')==make_pair('A','1'));
  assert(_board.find('R')==make_pair('0','0'));
  //test check function
  assert(_board.check(true));
  assert(_board.check(false));
  assert(_board.add_piece(make_pair('B','3'),'P'));
  assert(!_board.check(true));
  //test display
  */
  _board.display();
  
  std::cout<<_board;
  
  std::cout << "All assertions passed!" << std::endl;
  return 0;
}
