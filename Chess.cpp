#include "Chess.h"

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true )
{
	// Add the pawns
	for( int i=0 ; i<8 ; i++ )
	{
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
	}

	// Add the rooks
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
}

bool Chess::make_move( std::pair< char , char > start , std::pair< char , char > end )
{
  // start by getting piece pointers off board using () overload
  const Piece* moving_piece = _board(start);
  const Piece* end_piece = _board(end);
  
  // checking if there's a piece at start and if the piece can be moved on this turn
  // i.e. both moving_piece and _turn_white correspond to same "color"
  if(!moving_piece) {return false;}
  if (_turn_white) {
    if (!moving_piece->is_white()) {
      return false;
    }
  }
  if (!_turn_white) {
    if (moving_piece->is_white()) {
      return false;
    }
  }
  
  char mp_type = moving_piece->to_ascii();
  
  if(end_piece == nullptr) { // everything within executes only when ending square is unoccupied
    // checking if the move is one this piece can do
    bool isLegalMove = moving_piece->legal_move_shape(start, end);
    if(isLegalMove) {
      // checking if path is clear
      bool pathClear = _board.legal_path(start, end);
      if(pathClear) {
	/* this is the actual moving: adding the moving piece to board at end square
	 * and removing the original piece at start */
	_board.add_piece(end, mp_type);
	_board.remove_piece(start);

	/* if the move puts the current player in check, it is not allowed
	 * and must be undone */
	if(in_check(_turn_white)) {
	  _board.add_piece(start, mp_type);
	  _board.remove_piece(end);
	  return false; // should we print something about move causing check?
	}
	// only ever makes changes when promotion conditions met
	_board.pawn_promote(end);
	change_turn();
	// move complete
	return true;
      }
    }
  } else if(end_piece) { // the code in this if block exe's when the end square is occupied
    char ep_type = end_piece->to_ascii();
    // ensures that moving and end pieces are not of the same color
    if(end_piece->is_white() == moving_piece->is_white()) {return false;}
    // checking capture shape, which is the same in all cases save pawn
    bool isLegalCap = moving_piece->legal_capture_shape(start, end);
    if(isLegalCap) {
      // checking path
      bool pathClear = _board.legal_path(start, end);
      if(pathClear) {
	_board.remove_piece(end);
	_board.add_piece(end, mp_type);
	_board.remove_piece(start);
	// same as earlier, move undone if current player's king is in check
	if(in_check(_turn_white)) {
	  _board.add_piece(start, mp_type);
	  _board.remove_piece(end);
	  _board.add_piece(end, ep_type);
	  return false;
	}
	// promotion conditions check
	_board.pawn_promote(end);
	change_turn();
	// move complete
	return true;
      }
    }
  }
  // if for some reason we get here the move has not occured, returns false 
  return false;
}

// runs make move without moving piece
bool Chess::test_move( std::pair< char , char > start , std::pair< char , char > end ) const {
  // getting pointers to pieces at starting and ending squares
  //const Piece* mp = _board(start);
  //const Piece* ep = _board(end);
  Chess test(*this);
  // checking if starting square is occupied
  //if (!mp) { return false; }
  
  // getting piece types/checking if end square is occupied
  /* char end_name;
  if (ep) {
    end_name = ep -> to_ascii();
  }
  char piece_name = mp->to_ascii(); */
  
  // calling make_move with the given squares 
  return test.make_move(start, end);

  /* if make_move returns true then the move happened. The move must be
   * undone. The following if blocks do that i.e. change turn again and 
   * move piece(s) to their original squares */
  /* if(isLegal == true && ep == nullptr) {
    change_turn();
    _board.add_piece(start, piece_name);
    _board.remove_piece(end);
  } else if(isLegal == true && ep != nullptr) {
    change_turn();
    _board.add_piece(start, piece_name);
    _board.remove_piece(end);
    _board.add_piece(end, end_name);
    } */
  // the result of make_move is returned, but the board retains its condition from before
}

/* calls check function in Board.cpp; it's "easier" to look through a Chess's _board's _occ map
 * as a member function of Board than as a member function of Chess */
bool Chess::in_check( bool white ) const 
{
  return _board.check(white);
  /*
  std::pair<char,char> king;
  if (white) {
    king = _board.find('K');
    for ()
    }
  if (!white) {
    king = _board.find('k');
  }
  */
	//return result;
}

bool Chess::in_mate( bool white ) const
{
  // ASSUMING king is already in-check, no matter where king moves, king still in-check
  // ASK LINGXI The above statement might not be true, however this function does check
  // king's moves, so we'd be in the clear

  // if current player isn't in check then they can't be in mate
  if (!in_check(white)) {return false;}
  /*
    for (int i = -1; i <=1; i++) {
      for (int j = -1; j <=1; j++) {
        if (i == 0 && j == 0) {
          continue;
        }
        bool ifok = make_move();
        if (ifok) {
          return false;
        }
        //continue the process
      }
    }
  */
  // if any of the current player's pieces can remove check, then returns false
  // this loop checks every piece on the board for the current player's pieces
  for (char i = 'A'; i <= 'H'; i++) {
    for (char j = '1'; j <= '8'; j++) {
      // c is short for check; we're looking for pieces that can remove it
      const Piece* c_remover = _board(std::make_pair(i,j));

      // if no piece, continues looping
      if (c_remover == NULL) {
	continue;
      }
      
      // if c_remover is not the current player's piece (i.e. different color), continues looping
      if (white) {
	if (c_remover->to_ascii() < 'A' || c_remover->to_ascii() > 'Z') {
	  continue;
	}
      }
      if (!white) {
	if (c_remover->to_ascii() < 'a' || c_remover->to_ascii() > 'z') {
	  continue;
	}
      }

      // no_path tries to use piece at (i,j) to break check 
      bool no_hope = no_path(std::make_pair(i,j));

      /* if there is hope (i.e. no_path returns false, which means there IS a move that can 
       * break check) then in_mate return false */
      if (!no_hope) { 
	return false;
      }
    }
  }
  // if no_hope is always true no matter the piece, then current player is mated
  return true;
}

/* helper function for in_mate, takes piece at start and tries to move it to
 * any square on the board. Will only return false if the piece at start can 
 * move in such a way that removes check (as make_move and by extension test_move
 * return false if the move occurs and the current player's king is in check). */
bool Chess::no_path(std::pair<char,char> start) const {
  for (char i = 'A'; i <= 'H'; i++) {
    for (char j = '1'; j <= '8'; j++) {
      if (test_move(start, std::make_pair(i,j))) {
        return false;
      }
    }
  }
  return true;
}

// Functionally the same as in_mate with one major difference at the start
bool Chess::in_stalemate( bool white ) const
{
  // if current player is checked, they can't be stalemated
  if (in_check(white)) {return false;}
  //if no legal move for any pieces, return true
  for (char i = 'A'; i <= 'H'; i++) {
    for (char j = '1'; j <= '8'; j++) {
      const Piece* temp = _board(std::make_pair(i,j));
      if (temp == NULL) {
        continue;
      }
      //if it is white
      if (white) {
        if (temp->to_ascii() < 'A' || temp->to_ascii() > 'Z') {
          continue;
        }
      }
      if (!white) {
        if (temp->to_ascii() < 'a' || temp->to_ascii() > 'z') {
          continue;
        }
      }
      bool no_hope = no_path(std::make_pair(i,j));
      if (!no_hope) {
	return false;
      }
    }
  }
  return true;
}

void Chess::change_piece(std::istream& is) {
  if(is) {
    for (char i = '8'; i >= '1'; i--) {
      for (char j = 'A'; j <= 'H'; j++) {
	char temp;
	is >> temp;
	if (temp == '-') {
	  _board.remove_piece(std::pair< char , char > (j, i));
	  continue;
	}
	_board.remove_piece(std::pair< char , char > (j, i));
	_board.add_piece(std::pair< char , char > (j,i), temp);
	//_board.pawn_promote(std::pair< char , char > (j,i));
	//chess.board().remove_piece(std::make_pair(j,i));
	//cout << temp << " " << endl;
      }
    }
    //cout << *b <<endl;
    //chess.set_board(*b);



    char color;
    is >> color;
    
    if (color == 'b') {
      _turn_white = false;
    }
    if (color == 'w') {
      _turn_white = true;
    }
  }
}

void Chess::clear() {
  for (char i = '8'; i >= '1'; i--) {
    for (char j = 'A'; j <= 'H'; j++) {
      _board.remove_piece(std::pair< char , char > (j, i));
    }
  }
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
	// Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}


std::istream& operator >> ( std::istream& is , Chess& chess )
{

  chess.change_piece(is);
  
  /* if(is) {
    for (char i = '8'; i >= '1'; i--) {
      for (char j = 'A'; j <= 'H'; j++) {
	char temp;
	is >> temp;
	if (temp == '-') {
	  // chess.board().remove_piece(std::pair< char , char > (j, i));
	  continue;
	}
	chess.board().remove_piece(std::pair< char , char > (j, i));
	chess.board().add_piece(std::pair< char , char > (j,i), temp);
      }
    }
    char color;
    is >> color;
    
    if (color == 'b') {
      if(chess.turn_white() == true) {chess.change_turn();}
    }
    if (color == 'w') {
      if(chess.turn_white() == false) {chess.change_turn();}
    }
    } */
  return is;
}
