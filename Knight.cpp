// Knight.cpp

#include "Knight.h"

bool Knight::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  // check if the start and end positions are legal
  if (start.first < 'A' || start.first > 'H' ||
      start.second < '1' || start.second > '8' ||
      end.first < 'A' || end.first > 'H' ||
      end.second < '1' || end.second > '8') {
    return false;
  }
  
  // move:the columns moved and the rows moved must be either +/-1 or +/-2
  // also, columns moved and rows moves cannot be the same
  if (start.first - end.first == 1 || start.first - end.first == -1) {
    if (start.second - end.second == 2 || start.second - end.second == -2) { 
      return true;
    }
  }
  if (start.first - end.first == 2 || start.first - end.first == -2) {
    if (start.second - end.second == 1 || start.second - end.second == -1) {
      return true;
    }
  }
  // other moves are all illega
  return false;
}
