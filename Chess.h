#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include "Piece.h"
#include "Board.h"

class Chess
{
public:
	// This default constructor initializes a board with the standard piece positions, and sets the state to white's turn
	Chess( void );

	// Returns a constant reference to the board 
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	const Board& board( void ) const { return _board; }

	// Returns true if it's white's turn
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	bool turn_white( void ) const { return _turn_white; }

	// Called in make_move to change turn after succesful move
	// also called in test_move to change turn back after a successful move
	void change_turn( void ) { _turn_white = !_turn_white; }
	
	// destructor, calls clear to deallocate memory allocated by create_piece calls
	~Chess(void) {/* clear(); */}

	// removes pieces from board using remove_piece; remove_piece deallocates pieces in _board._occ
	void clear(void);

	// Attemps to make a move. If successful, the move is made and the turn is switched white <-> black
	bool make_move( std::pair< char , char > start , std::pair< char , char > end );

	// Returns true if the designated player is in check
	bool in_check( bool white ) const;

	// Returns true if the designated player is in mate
	bool in_mate( bool white ) const;

	// Returns true if the designated player is in stalemate
	bool in_stalemate( bool white ) const;

	/* used by in_mate. given a start location (which should be occupied) calls test_move
	 * for every other square on the board. Since the current player's king is in check,
	 * the only legal moves are ones that remove check. If any moves are legal, false is
	 * returned. If there are no legal moves, then there is "no path" out of check and 
	 * true is returned. */
	bool no_path(std::pair<char,char> start) const;

	// same as make_move, but without actually making the move. returns true when move is possible
	bool test_move( std::pair< char , char > start , std::pair< char , char > end ) const;

	/* used by extraction operator overloader for Chess objects to populate the chess
	 * object's board and set the turn as given by the input stream i.e. text file */
	void change_piece(std::istream& is);
private:
	// The board
	Board _board;

	// Is it white's turn?
	bool _turn_white;
};

// Writes the board out to a stream
std::ostream& operator << ( std::ostream& os , const Chess& chess );

// Reads the board in from a stream
std::istream& operator >> ( std::istream& is , Chess& chess );


#endif // CHESS_H
