// Queen.cpp

#include "Queen.h"

bool Queen::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  // check if the start and end positions are legal
  if (start.first < 'A' || start.first > 'H' ||
      start.second < '1' || start.second > '8' ||
      end.first < 'A' || end.first > 'H' ||
      end.second < '1' || end.second > '8') {
    return false;
  }
  // check if the move shape is horizontal or vertical
  // i.e. either the row or column remains the same
  if (start.first == end.first || start.second == end.second) {
    return true;
  }
  // check if the move shape is diagonal
  // i.e. the difference in rows and columns is the same
  if (abs(start.first - end.first) == abs(start.second - end.second)) {
    return true;
  }
  // all other shapes are illegal
  return false;
}
