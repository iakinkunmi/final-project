//test1.cpp

#include <iostream>
#include <cassert>
#include "Rook.h"
#include "King.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "Pawn.h"
#include "CreatePiece.h"


using std::make_pair; using std::pair;

int main() {

  // Piece *k = create_piece('k');
  //Piece *r = create_piece('r');
  Piece *b = create_piece('b');
  //Piece *q = create_piece('q');
  /* Piece *n = create_piece('n');
  Piece *p = create_piece('p');
  pair<char, char> sk = make_pair('B','2');
  pair<char, char> ek = make_pair('A','3');
  pair<char, char> sr = make_pair('B','2');
  pair<char, char> er = make_pair('B','5');
  pair<char, char> sb = make_pair('B','2');
  pair<char, char> eb = make_pair('A','3');
  pair<char, char> sq = make_pair('B','2');
  pair<char, char> eq = make_pair('A','3');
  pair<char, char> sn = make_pair('D','4');
  pair<char, char> en = make_pair('E','6');
  pair<char, char> sp = make_pair('A','3');
  pair<char, char> ep = make_pair('A','4');
  assert((k->legal_move_shape(sk, ek)));
  assert((r->legal_move_shape(sr, er)));
  assert((b->legal_move_shape(sb, eb)));
  assert((q->legal_move_shape(sq, eq)));
  assert((n->legal_move_shape(sn, en)));
  assert((p->legal_move_shape(sp, ep)));

  assert(p->legal_capture_shape(make_pair('B','7'), make_pair('A','6')));
  Piece *P = create_piece('P');
  assert(P->legal_capture_shape(make_pair('D','2'), make_pair('C','3')));
  */
  std::cout << "All assertions passed!" << std::endl;
}
